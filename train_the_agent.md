# Train the Agent

In this section, we will show you how to train a new model for the agent. The default pre-trained model provided in the package only performs basic movement, so we will show you how you can unlock the potential using the built-in Reinforcement Learning algorithm.

## How does the training work?
This training uses the Reinforcement Learning algorithm to train our agent. Put it simply if you will, it's another form of learning through reward and penalty. The agent will continuously making decision on how to interact with the environment using the input from the sensors (observation). The agent will perform a series of actions, get rewarded or penalised depending on the outcome. In this case, the agent gets rewarded if it found the cheese and penalised for hitting a cat. 

![Reinforcement_Learning](images/rl_cycle.png)

The goal of this training is to get the agent to learn a policy. The policy is a mapping from the observations of the environment to actions. Consider this as what the agent needs to do in order to get closer to the cheese. The observation is what the agent can measure or detect from the environment, such as the presence of a cat, the wall, the switches and lastly the cheese. The action is the changes to the movement and direction of the agent, such as move forward or rotate clockwise. 

Finally, is the reward signal. The rewards (positive and negative) values are provided during the training to indicate how well the agent is doing in completing the task. The agent does not know the objective and how to get to the cheese until it receives a positive reward when it found the cheese. The agent also learns to optimise the policy by getting a small penalty for every passing seconds, in other words, the faster it completes the task the lesser penalty it gets.

---

## Setting Reward and Penalty
Before we begin to train the agent, let's go through the reward and penalty parameters. How you set the values will decide the outcome. The parameters are the following.

1. End Reward (The agent will be rewarded if it found the cheese. This value must be > 0). 
2. Switch reward (The agent will be rewarded if it hits the switch and switch colour is the same as the agent. This value must be > 0 and usually lower than the End Reward).
3. Fail reward (The agent will be penalised if it fails to get to the cheese after a certain amount of time. This value must be < 0. This value will encourage the agent to build an optimal policy to complete the task faster).
4. Fail by Cat reward (The agent will be penalised if it hits a cat. This value must be < 0.).

![RewardandPenalty](images/reward_penalty.png)

---

## Start the training
You can begin to train your agent after setting the reward and penalty values.

1. Navigate to folder where the **ml-agents-release_17**.
2. Open the command window or terminal.
3. Run `mlagents-learn config/ppo/CatAndMouse.yaml --run-id=CatAndMouseRun`.

   - **config/ppo/CatAndMouse.yaml** is the path to the training configuration file.
   - **run-id** is a unique name for this training session.
   
   **Note:** If you see this message **"Previous data from this run ID was found"**, that means you need to assign a new run-id to continue.

   ![Training](images/training_4.png)

4. When you see the message **"Start training by pressing the Play button in the Unity Editor"** is displayed on the screen, you can press the **Play** button in Unity to start training in the Editor.

![Training](images/training_1.png)

You will see something like this if the training runs correctly.

![Training](images/training_2.png)

---

## Observe the progress
After the training has been running a while, you can observe the progress using a very useful tool called TensorBoard. This allows you to observer the training with more detail. To enable the Tensorboard, open a new terminal or command window and run the command below.
> tensorboard --logdir results

Type `localhost:6006` into your web browser and you will see something like this.

![Observer](images/graph.png)

If you leave it for an hour or more, your graph should look similar to the image below.  

![Observer](images/graph_good.png)

---

## Integrate the trained model
Once the training process completes, and the training process saves the model (denoted by the Saved Model message) you can add it to the Unity project and use it with compatible Agents (the Agents that generated the model). Note: Do not just close the Unity Window once the Saved Model message appears. Either wait for the training process to close the window or press Ctrl+C at the command-line prompt. If you close the window manually, the .onnx file containing the trained model is not exported into the ml-agents folder.

Your trained model will be at **results/<run-identifier>/CatAndMouse.onnx**. This file corresponds to your model's latest checkpoint. You can now embed this trained model into your Agents by following the steps below, which is similar to the steps described above.

1. Move your model file into **Project/Assets/CatAndMouse/TFModels/**.
2. Open the Unity Editor, and select the **CatAndMouse.unity** scene as described above.
3. Select the Mouse prefab Agent object.
4. Drag the **CatAndMouse.onnx** file from the Project window of the Editor to the Model placeholder in the CatAndMouseAgent inspector window.

![Observer](images/training_3.png)

5. Press the **Play** button and observe the mouse.