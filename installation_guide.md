# Installation Guide

## Install Unity 2020.2.5f1
Download and run the installation file (download link [here](https://unity3d.com/get-unity/download?thank-you=update&download_nid=64491&os=Win)).

---

## Install Python (version 3.6.1 or higher)
1. Download and run the installation file (download link [here](https://www.python.org/ftp/python/3.7.9/python-3.7.9-amd64.exe)).

2. **Important!** Make sure you have checked **`"Add Python 3.7 to PATH"`** as shown in the images below before you proceed.

![Python installtion](images/python_1.png)

3. Click on the **`"Install Now"`** to begin the installation.

![Python installtion](images/python_2.png)

4. You may close this screen after the Python has been successfully installed.

![Python installtion](images/python_3.png)

5. To the Python environment, press the **`"Windows key + R"`** and type **`cmd`**. Click on the "ok" button or press "Enter" key to proceed.

![Python installtion](images/run_window.png)

6. You will see a command prompt window as shown in the image below. Type the following command into the command prompt
> python --version

![Python installtion](images/cmd_prompt_2.png)

7. You will see the Python version number display after you have input the command. If you don't see the version number, please restart the Python installation from Step 1.

![Python installtion](images/cmd_prompt.png)

---

## Download Game Lab ML-Agents Toolkit
Now that you have installed Unity and Python, you can start to install the core components from in the Game Lab ML-Agents Toolkit. The toolkit contains the libraries required to train you agent later. 

1. Download the toolkit from [here](https://drive.google.com/file/d/1aIQsXjHqYfVboD4dyXrp4Q_FdB5QCA__/view?usp=sharing).

2. Unzip the downloaded file into the **`"Documents"`** folder. You may choose to unzip the file into another folder.

3. Now is the good time to open Unity project. Locate the **`"CatAndMouse.unity"`** file in this location **`"ml-agents-release_17\Project\Assets\CatAndMouse\Scenes"`** from the folder you have unzip ealier.

4. Open the **`"CatAndMouse.unity"`** file and it will launch the Unity editor.

---

## Install com.unity.ml-agents package
1. Open package manager in the Unity editor.

![com.unity.ml-agents package](images/unity_package_1.png)

2. Click on the "+" (plus sign) and choose "Add package from disk".

![com.unity.ml-agents package](images/unity_package_2.png)

3. Locate the "package.json" from the location as shown in the image below. Click the "Open" button after you have selected the file.

![com.unity.ml-agents package](images/unity_package_3.png)

4. You will see the "ML Agents" and "ML Agents Extension" in the list.

![com.unity.ml-agents package](images/unity_package_4.png)

---

## Install mlagents Python package
The mlagents Python package creates a virtual environment and enables the trainer console to train your agent. 

### (For Windows user only)
You must run this command to install PyTorch prior to installing the mlagents Python package if your are using Windows.
> pip3 install torch~=1.7.1 -f https://download.pytorch.org/whl/torch_stable.html

**NOTE:** You may get this message **`"WARNING: You are using pip version 20.1.1; however, version 21.1.3 is available."`** after you have successfully installed the PyTorch. You can ignore this message.

**NOTE:** You may be prompt to install **`Microsoft's Visual C++ Redistributable`** if you don't have it already.

Run the command below to install the mlagents package
> python -m pip install mlagents==0.26.0

**NOTE:** You may get this message **`"WARNING: You are using pip version 20.1.1; however, version 21.1.3 is available."`** after you have successfully installed the PyTorch. You can ignore this message.

If you installed the mlagents package correctly, you should be able to run 
> mlagents-learn --help 

and you will see the command line parameters you can use with mlagents-learn

---

## Test the environment
You can test the ML-agents environment using the CatAndMouse.unity that you have opened earlier. Press the "Play" button to run the scene. If you have installed the ML-agents toolkit correctly, you should see the same action in the animated GIFs below.

![3D ball](images/agent_2.gif)