# Get the Cheese!

Get the Cheese is game made by SUTD Game Lab as a starter package to introduce you to machine learning for games. This page will guide you through the installation and setting the environment for training the agent. This project uses the components from the Unity ML-Agents Toolkit by Unity Technologies as the foundation (you can read more about their implementation [here](https://github.com/Unity-Technologies/ml-agents)).

## About this game
Get the Cheese is a simple 3D puzzle game made to demonstrate how an intelligent agent can be trained to explore and solve a puzzle within the environment. As mentioned in the title, the goal of this game is to get your mouse (the agent) to the cheese. The puzzle element requires the mouse to activate the right switch to reveal the cheese. The game ends when the mouse gets the cheese or bumps into a cat.

## Getting Started
### 1. Requirements
- OS: Windows 10 64-bit.
- GPU: Graphics card with DX10 (shader model 4.0) capabilities.
- Unity3D 2020.2.5f1.
### 2. Installation Guide
- [Install Unity3D and setting up training environment](installation_guide.md)
### 3. Setting up the game
- [Train the agent](train_the_agent.md)
### 4. Extra Training Environment
- [Map 2](https://drive.google.com/file/d/1rtN1s5__OMxcp_mvarHAYC3DhZ5mOulN/view?usp=sharing)
- [Map 3](https://drive.google.com/file/d/1TzTMbe09OSxmnwaiRttnH5jjJsTFbMig/view?usp=sharing)

## Additional Reading

- [Reinforcement Learning](https://towardsdatascience.com/reinforcement-learning-101-e24b50e1d292#:~:text=Reinforcement%20Learning(RL)%20is%20a,its%20own%20actions%20and%20experiences.)
- [A Beginner’s Guide To Reinforcement Learning With A Mario Bros Example](https://towardsdatascience.com/a-beginners-guide-to-reinforcement-learning-with-a-mario-bros-example-fa0e0563aeb7)
- [The Ultimate Beginner’s Guide to Reinforcement Learning](https://towardsdatascience.com/the-ultimate-beginners-guide-to-reinforcement-learning-588c071af1ec)
- [Making a DotA2 Bot Using ML](https://towardsdatascience.com/making-a-dota2-bot-using-ml-c8cb2423a2be)
- [Quickly Training Game-Playing Agents with Machine Learning](https://ai.googleblog.com/2021/06/quickly-training-game-playing-agents.html)